package com.lydia.uniguideapi.api;

import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lydia.uniguideapi.domains.Career;
import com.lydia.uniguideapi.domains.News;
import com.lydia.uniguideapi.services.CareerService;
import com.lydia.uniguideapi.services.NewsService;

@RestController
@RequestMapping(path="/career", produces="application/json")
public class CareersController {

	private CareerService careerService;
	
	public CareersController(CareerService careerService) {
		this.careerService = careerService;
	}
	
	@GetMapping("/all")
	public Iterable<Career> recentNews() {

		return careerService.findAll();
	}
	
	@GetMapping("/{idVal}")
	public ResponseEntity<Career> newsById(@PathVariable("idVal") String id){
		Optional<Career> optCareer = careerService.findById(id);
		if(optCareer.isPresent()) {
			return new ResponseEntity<>(optCareer.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(path="/save", consumes="application/json")
	public ResponseEntity<Career> saveCareer(@RequestBody Career career) {
		System.out.println("career: " + career);
		Career savedCareer = careerService.save(career);
		if(savedCareer != null) {
			return new ResponseEntity<>(savedCareer, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
	}
	
	@PutMapping(path="/{careerId}", consumes="application/json")
	public ResponseEntity<Career> wholesaleCareerUpdate(@RequestBody Career career) {
		Career savedCareer = careerService.save(career);
		if(savedCareer != null) {
			return new ResponseEntity<>(savedCareer, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
	}
	
	@PatchMapping(path="/{careerId}", consumes="application/json")
	public ResponseEntity<Career> partialCareerUpdate(@PathVariable("careerId") String id, @RequestBody Career partialCareer) {
		
		Career career = careerService.findById(id).get();
		
		if(career != null) {
			if(partialCareer.getGroup_name() != null) {
				career.setGroup_name(partialCareer.getGroup_name());
			}
			
			if(partialCareer.getGroup_jobs()!=null) {
				career.setGroup_jobs(partialCareer.getGroup_jobs());
			}
			
			if(partialCareer.getGroup_description()!=null) {
				career.setGroup_description(partialCareer.getGroup_description());
			}
			
		}
		
		Career savedCareer = careerService.save(career);
		if(savedCareer != null) {
			return new ResponseEntity<>(savedCareer, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
	}
	@DeleteMapping(path="/{careerId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCareer(@PathVariable("careerId") String id){
		try {
			careerService.deleteById(id);
		}catch(EmptyResultDataAccessException e) {
			
		}
		
	}
}
