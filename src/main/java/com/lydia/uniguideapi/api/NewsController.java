package com.lydia.uniguideapi.api;

import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lydia.uniguideapi.domains.News;
import com.lydia.uniguideapi.services.NewsService;


@RestController
@RequestMapping(path="/news", produces="application/json")
public class NewsController {

	private NewsService newsService;
	
	public NewsController(NewsService newsService) {
		this.newsService = newsService;
	}
	
	@GetMapping("/all")
	public Iterable<News> recentNews() {

		return newsService.findAll();
	}
	
	@GetMapping("/{idVal}")
	public ResponseEntity<News> newsById(@PathVariable("idVal") Long id){
		Optional<News> optNews = newsService.findById(id);
		if(optNews.isPresent()) {
			return new ResponseEntity<>(optNews.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(path="/save", consumes="application/json")
	public ResponseEntity<News> saveNews(@RequestBody News news) {
		News savedNews = newsService.save(news);
		if(savedNews != null) {
			return new ResponseEntity<>(savedNews, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
	}
	
	@PutMapping(path="/{newsId}", consumes="application/json")
	public ResponseEntity<News> wholesaleNewsUpdate(@RequestBody News news) {
		News savedNews = newsService.save(news);
		if(savedNews != null) {
			return new ResponseEntity<>(savedNews, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
	}
	
	@PatchMapping(path="/{newsId}", consumes="application/json")
	public ResponseEntity<News> partialNewsUpdate(@PathVariable("newsId") Long id, @RequestBody News partialNews) {
		
		News news = newsService.findById(id).get();
		
		if(news != null) {
			if(partialNews.getContent()!= null) {
				news.setContent(partialNews.getContent());
			}
			
			if(partialNews.getTitle()!=null) {
				news.setTitle(partialNews.getTitle());
			}
			
			if(partialNews.getAuthor()!=null) {
				news.setAuthor(partialNews.getAuthor());
			}
			
		}
		
		News savedNews = newsService.save(news);
		if(savedNews != null) {
			return new ResponseEntity<>(savedNews, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
	}
	@DeleteMapping(path="/{newsId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteNews(@PathVariable("newsId") Long id){
		try {
			newsService.deleteById(id);
		}catch(EmptyResultDataAccessException e) {
			
		}
		
	}
}
	

