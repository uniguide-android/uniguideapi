package com.lydia.uniguideapi.api;

import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lydia.uniguideapi.domains.Post;
import com.lydia.uniguideapi.services.PostService;

@RestController
@RequestMapping(path="/post", produces="application/json")
public class PostsController {

	private PostService postService;
	
	public PostsController(PostService postService) {
		this.postService = postService;
	}
	
	@GetMapping("/all")
	public Iterable<Post> posts(@RequestParam(name="type", defaultValue="universities") String type) {
		
		return postService.findByType(type);
	}
	
	@GetMapping("/{idVal}")
	public ResponseEntity<Post> postById(@PathVariable("idVal") Long id){
		Optional<Post> optPost = postService.findById(id);
		if(optPost.isPresent()) {
			return new ResponseEntity<>(optPost.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(path="/save", consumes="application/json")
	public ResponseEntity<Post> savePost(@RequestBody Post post) {
		Post savedPost = postService.save(post);
		if(savedPost != null) {
			return new ResponseEntity<>(savedPost, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PutMapping(path="/{postId}", consumes="application/json")
	public ResponseEntity<Post> wholesalePostUpdate(@RequestBody Post post) {
		Post savedPost = postService.save(post);
		if(savedPost != null) {
			return new ResponseEntity<>(savedPost, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PatchMapping(path="/{postId}", consumes="application/json")
	public ResponseEntity<Post> partialPostUpdate(@PathVariable("postId") Long id, @RequestBody Post partialPost) {
		
		Post post = postService.findById(id).get();
		
		if(post != null) {
			if(partialPost.getContent()!= null) {
				post.setContent(partialPost.getContent());
			}
		
			if(partialPost.getLikes() != 0) {
				post.setLikes(partialPost.getLikes());
			}
			
			if(partialPost.getFlag() != null) {
				post.setFlag(partialPost.getFlag());
			}
			
			if(partialPost.getType() != null) {
				post.setType(partialPost.getType());
			}
		}
		
		Post savedPost = postService.save(post);
		if(savedPost != null) {
			return new ResponseEntity<>(savedPost, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	@DeleteMapping(path="/{postId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletePost(@PathVariable("postId") Long id){
		try {
			postService.deleteById(id);
		}catch(EmptyResultDataAccessException e) {
			
		}
		
	}
}
	

