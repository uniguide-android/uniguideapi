package com.lydia.uniguideapi.api;

import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lydia.uniguideapi.domains.Agent;
import com.lydia.uniguideapi.domains.Student;
import com.lydia.uniguideapi.domains.User;
import com.lydia.uniguideapi.services.AgentService;
import com.lydia.uniguideapi.services.UserService;

@RestController
@RequestMapping(path="/agent", produces="application/json")
public class AgentsController {

	private AgentService agentService;
	private UserService userService;
	
	public AgentsController(AgentService agentService, UserService userService) {
		this.userService = userService;
		this.agentService = agentService;
	}
	
	@GetMapping("/all")
	public Iterable<Agent> agents() {
		
		return agentService.findAll();
	}
	
	@GetMapping("/{idVal}")
	public ResponseEntity<Agent> agentById(@PathVariable("idVal") Long id){
		Optional<Agent> optAgent = agentService.findById(id);
		if(optAgent.isPresent()) {
			return new ResponseEntity<>(optAgent.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/user/{idVal}")
	public ResponseEntity<Agent> agentByUserId(@PathVariable("idVal") Long id){
		Optional<Agent> optAgent = agentService.findByUserId(id);
		if(optAgent.isPresent()) {
			return new ResponseEntity<>(optAgent.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(path="/register", consumes="application/json")
	public ResponseEntity<Agent> registerAgent(@RequestBody Agent agent) {
		System.out.println("request agent: " + agent);
		User user = agent.getUser();
		User savedUser = userService.saveUser(user, "AGENT");
		System.out.println("saved user: " + savedUser);
		agent.setUser(savedUser);
		System.out.println("agent with user: " + agent);
		Agent savedAgent = agentService.save(agent);
		System.out.println("saved agent: " + savedAgent);
		if(savedUser != null && savedAgent != null) {			
			return new ResponseEntity<>(agent, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
	}
	
	@PatchMapping(path="/{agentId}", consumes="application/json")
	public ResponseEntity<Agent> partialAgentUpdate(@PathVariable("agentId") Long id, @RequestBody Agent partialAgent) {
		
		Agent agent = agentService.findById(id).get();
		
		if(agent != null) {
			if(partialAgent.getName()!= null) {
				agent.setName(partialAgent.getName());
			}
			
			if(partialAgent.getEmail()!=null) {
				agent.setEmail(partialAgent.getEmail());
			}
			
			if(partialAgent.getPhoneNo() != null) {
				agent.setPhoneNo(partialAgent.getPhoneNo());
			}
			
			if(partialAgent.getAddress() != null) {
				agent.setAddress(partialAgent.getAddress());
			}
			
			if(partialAgent.getDescription() != null) {
				agent.setDescription(partialAgent.getDescription());
			}
			
		}
		
		Agent savedAgent = agentService.save(agent);
		if(savedAgent != null) {
			return new ResponseEntity<>(savedAgent, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
	}
	@DeleteMapping(path="/{agentId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteAgent(@PathVariable("agentId") Long id){
		try {
			agentService.deleteById(id);
		}catch(EmptyResultDataAccessException e) {
			
		}
		
	}
}
