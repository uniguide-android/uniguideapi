package com.lydia.uniguideapi.api;

import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lydia.uniguideapi.domains.Student;
import com.lydia.uniguideapi.domains.User;
import com.lydia.uniguideapi.services.StudentService;
import com.lydia.uniguideapi.services.UserService;

@RestController
@RequestMapping(path="/student", produces="application/json")
public class StudentsController {


	private StudentService studentService;
	private UserService userService;
	
	public StudentsController(StudentService studentService, UserService userService) {
		this.studentService = studentService;
		this.userService = userService;
	}
	
	@GetMapping("/all")
	public Iterable<Student> students() {
		
		return studentService.findAll();
	}
	
	@GetMapping("/{idVal}")
	public ResponseEntity<Student> studentById(@PathVariable("idVal") Long id){
		Optional<Student> optStudent = studentService.findById(id);
		if(optStudent.isPresent()) {
			return new ResponseEntity<>(optStudent.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/user/{idVal}")
	public ResponseEntity<Student> studentByUserId(@PathVariable("idVal") Long id){
		Optional<Student> optStudent = studentService.findByUserId(id);
		if(optStudent.isPresent()) {
			return new ResponseEntity<>(optStudent.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(path="/register", consumes="application/json")
	public ResponseEntity<Student> registerStudent(@RequestBody Student student) {
		User user = student.getUser();
		User savedUser = userService.saveUser(user, "STUDENT");
		student.setUser(savedUser);
		System.out.println("student with user: " + student);
		Student savedStudent = studentService.save(student);
		if(savedUser != null && savedStudent != null) {			
			return new ResponseEntity<>(student, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PutMapping(path="/{studentId}", consumes="application/json")
	public ResponseEntity<Student> wholesaleTacoUpdate(@RequestBody Student student) {
		Student savedStudent = studentService.save(student);
		if(savedStudent != null) {			
			return new ResponseEntity<>(student, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		 
	}
	
	@PatchMapping(path="/{studentId}", consumes="application/json")
	public ResponseEntity<Student> partialStudentUpdate(@PathVariable("studentId") Long id, @RequestBody Student partialStudent) {
		
		Student student = studentService.findById(id).get();
		
		if(student != null) {
			if(partialStudent.getName()!= null) {
				student.setName(partialStudent.getName());
			}
			
			if(partialStudent.getEmail()!=null) {
				student.setEmail(partialStudent.getEmail());
			}
			
			if(partialStudent.getSchool() != null) {
				student.setSchool(partialStudent.getSchool());
			}
			
			if(partialStudent.getGrade() != null) {
				student.setGrade(partialStudent.getGrade());
			}
						
			if(partialStudent.getAbout() != null) {
				student.setAbout(partialStudent.getAbout());
			}
		}
		
		Student savedStudent = studentService.save(student);
		if(savedStudent != null) {			
			return new ResponseEntity<>(student, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		 
	}
	@DeleteMapping(path="/{studentId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteStudent(@PathVariable("studentId") Long id){
		try {
			studentService.deleteById(id);
		}catch(EmptyResultDataAccessException e) {
			
		}
		
	}
}
