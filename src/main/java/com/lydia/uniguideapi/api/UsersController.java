package com.lydia.uniguideapi.api;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lydia.uniguideapi.domains.Agent;
import com.lydia.uniguideapi.domains.Post;
import com.lydia.uniguideapi.domains.Student;
import com.lydia.uniguideapi.domains.University;
import com.lydia.uniguideapi.domains.User;
import com.lydia.uniguideapi.services.AgentService;
import com.lydia.uniguideapi.services.StudentService;
import com.lydia.uniguideapi.services.UniversityService;
import com.lydia.uniguideapi.services.UserService;

@RestController
@RequestMapping(path="/", produces="application/json")
public class UsersController {

	private UserService userService;
	private StudentService studentService;
	private AgentService agentService;
	private UniversityService universityService;
	
	public UsersController(UserService userService, StudentService studentService, AgentService agentService, UniversityService universityService) {
		this.userService = userService;
		this.studentService = studentService;
		this.agentService = agentService;
		this.universityService = universityService;
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<User> userById(@PathVariable("id") Long id){
		Optional<User> user = userService.findById(id);
		if(user.isPresent()) {
			return new ResponseEntity<>(user.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/login")
	public ResponseEntity<User> login(@RequestParam(name="username", defaultValue="") String username, @RequestParam(name="password", defaultValue="") String password){
		Optional<User> user = userService.findUserByUsername(username);
		
		if(user.isPresent() && user.get().getPassword().equals(password)) {
			
			return new ResponseEntity<>(user.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
		

}