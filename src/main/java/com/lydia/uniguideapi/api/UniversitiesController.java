package com.lydia.uniguideapi.api;

import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lydia.uniguideapi.domains.Agent;
import com.lydia.uniguideapi.domains.University;
import com.lydia.uniguideapi.domains.User;
import com.lydia.uniguideapi.services.UniversityService;
import com.lydia.uniguideapi.services.UserService;

@RestController
@RequestMapping(path="/university", produces="application/json")
public class UniversitiesController {

	private UniversityService universityService;
	private UserService userService;
	
	public UniversitiesController(UniversityService universityService, UserService userService) {
		this.universityService = universityService;
		this.userService = userService;
	}
	
	@GetMapping("/all")
	public Iterable<University> universitys() {
		
		return universityService.findAll();
	}
	
	@GetMapping("/{idVal}")
	public ResponseEntity<University> universityById(@PathVariable("idVal") Long id){
		Optional<University> optUniversity = universityService.findById(id);
		if(optUniversity.isPresent()) {
			return new ResponseEntity<>(optUniversity.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/user/{idVal}")
	public ResponseEntity<University> universityByUserId(@PathVariable("idVal") Long id){
		Optional<University> optUniversity = universityService.findByUserId(id);
		if(optUniversity.isPresent()) {
			return new ResponseEntity<>(optUniversity.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@PostMapping(path="/register", consumes="application/json")
	public ResponseEntity<University> registerUniversity(@RequestBody University university) {
		User user = university.getUser();
		User savedUser = userService.saveUser(user, "UNIVERSITY");
		university.setUser(savedUser);
		University savedUniversity = universityService.save(university);
		if(savedUser != null && savedUniversity != null) {			
			return new ResponseEntity<>(university, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
	}
	
	@PatchMapping(path="/{universityId}", consumes="application/json")
	public ResponseEntity<University> partialUniversityUpdate(@PathVariable("universityId") Long id, @RequestBody University partialUniversity) {
		
		University university = universityService.findById(id).get();
		
		if(university != null) {
			if(partialUniversity.getName()!= null) {
				university.setName(partialUniversity.getName());
			}
			
			if(partialUniversity.getWebsite()!=null) {
				university.setWebsite(partialUniversity.getWebsite());
			}
			
			if(partialUniversity.getPhoneNo() != null) {
				university.setPhoneNo(partialUniversity.getPhoneNo());
			}
			
			if(partialUniversity.getDescription() != null) {
				university.setDescription(partialUniversity.getDescription());
			}
			
		}
		
		University savedUniversity = universityService.save(university);
		if(savedUniversity != null) {
			return new ResponseEntity<>(savedUniversity, HttpStatus.OK);
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping(path="/{universityId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUniversity(@PathVariable("universityId") Long id){
		try {
			universityService.deleteById(id);
		}catch(EmptyResultDataAccessException e) {
			
		}
		
	}
}
