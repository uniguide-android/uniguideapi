package com.lydia.uniguideapi.domains;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Table(name="career_groups")
public class Career {

	@Id
	private String group_code;
	
	public String group_name;
	
	private String group_jobs;
	
	private String group_description;
	
	
			
}

