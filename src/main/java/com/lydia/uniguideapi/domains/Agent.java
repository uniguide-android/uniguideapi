package com.lydia.uniguideapi.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name="agent")
public class Agent {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private  long id;
	
	private String name;
	
	private String email;
	
	private String phoneNo;
	
	private String address;
	
	private String description;
	
	@OneToOne
	private User user;

}
