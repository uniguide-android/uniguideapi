package com.lydia.uniguideapi.repositories;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.lydia.uniguideapi.domains.Agent;

public interface AgentRepository extends CrudRepository<Agent ,Long> {

	Optional<Agent> findByUserId(Long user_id);
}

