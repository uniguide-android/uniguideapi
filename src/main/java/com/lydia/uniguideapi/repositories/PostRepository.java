package com.lydia.uniguideapi.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.lydia.uniguideapi.domains.Post;

public interface PostRepository extends  PagingAndSortingRepository<Post ,Long> {

	Iterable<Post> findByType(String type);
}
