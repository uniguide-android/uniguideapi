package com.lydia.uniguideapi.repositories;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.lydia.uniguideapi.domains.Student;

public interface StudentRepository extends CrudRepository<Student ,Long> {

	Optional<Student> findByUserId(Long user_id);
}
