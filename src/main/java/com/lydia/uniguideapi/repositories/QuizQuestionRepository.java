package com.lydia.uniguideapi.repositories;

import org.springframework.data.repository.CrudRepository;

import com.lydia.uniguideapi.domains.QuizQuestion;

public interface QuizQuestionRepository extends CrudRepository<QuizQuestion ,String> {


}
