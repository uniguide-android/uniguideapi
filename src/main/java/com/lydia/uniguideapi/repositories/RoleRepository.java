package com.lydia.uniguideapi.repositories;

import org.springframework.data.repository.CrudRepository;

import com.lydia.uniguideapi.domains.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{

	Role findByRole(String role);
}
