package com.lydia.uniguideapi.repositories;

import org.springframework.data.repository.CrudRepository;

import com.lydia.uniguideapi.domains.News;

public interface NewsRepository extends CrudRepository<News ,Long> {


}