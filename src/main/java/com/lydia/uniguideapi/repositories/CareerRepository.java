package com.lydia.uniguideapi.repositories;
import org.springframework.data.repository.CrudRepository;

import com.lydia.uniguideapi.domains.Career;

public interface CareerRepository extends CrudRepository<Career ,String> {


}
