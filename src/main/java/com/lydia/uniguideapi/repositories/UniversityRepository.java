package com.lydia.uniguideapi.repositories;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.lydia.uniguideapi.domains.University;

public interface UniversityRepository extends CrudRepository<University ,Long> {

	Optional<University> findByUserId(Long user_id);
}