package com.lydia.uniguideapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniguideApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniguideApiApplication.class, args);
	}

}
