package com.lydia.uniguideapi.services;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lydia.uniguideapi.domains.Student;
import com.lydia.uniguideapi.repositories.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService {
	@Autowired
	StudentRepository studentRepository;
	
	
	
	@Override
	public Student save(Student student) {
		
		return studentRepository.save(student);
		
	}

	@Override
	public Iterable<Student> saveAll(Iterable<Student> students) {
		
		return studentRepository.saveAll(students);
		
	}

	@Override
	public Optional<Student> findById(Long id) {

		return studentRepository.findById(id);
	}
	@Override
	public Optional<Student> findByUserId(Long user_id) {

		return studentRepository.findByUserId(user_id);
	}

	@Override
	public boolean existsById(Long id) {
		
		return studentRepository.existsById(id);
		
	}

	@Override
	public Iterable<Student> findAll() {
		
		return studentRepository.findAll();
		
	}

	@Override
	public Iterable<Student> findAllById(Iterable<Long> ids) {

		return studentRepository.findAllById(ids);
		
	}

	@Override
	public long count() {
		
		return studentRepository.count();
		
	}

	@Override
	public void deleteById(Long id) {
		
		studentRepository.deleteById(id);
		
	}

	@Override
	public void delete(Student student) {
		
		studentRepository.delete(student);

	}

	@Override
	public void deleteAll(Iterable<Student> students) {

		studentRepository.deleteAll(students);
		
	}

	@Override
	public void deleteAll() {

		studentRepository.deleteAll();
		
	}

}