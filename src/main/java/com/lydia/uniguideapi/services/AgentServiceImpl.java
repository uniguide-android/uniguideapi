package com.lydia.uniguideapi.services;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lydia.uniguideapi.domains.Agent;
import com.lydia.uniguideapi.repositories.AgentRepository;


@Service
public class AgentServiceImpl implements AgentService {
	@Autowired
	AgentRepository agentRepository;
	
	
	
	
	@Override
	public Agent save(Agent agent) {
		System.out.println("agent to save: " + agent);
		return agentRepository.save(agent);
		
	}

	@Override
	public Iterable<Agent> saveAll(Iterable<Agent> agents) {
		
		return agentRepository.saveAll(agents);
		
	}

	@Override
	public Optional<Agent> findById(Long id) {

		return agentRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		
		return agentRepository.existsById(id);
		
	}

	@Override
	public Iterable<Agent> findAll() {
		
		return agentRepository.findAll();
		
	}

	@Override
	public Iterable<Agent> findAllById(Iterable<Long> ids) {

		return agentRepository.findAllById(ids);
		
	}

	@Override
	public long count() {
		
		return agentRepository.count();
		
	}

	@Override
	public void deleteById(Long id) {
		
		agentRepository.deleteById(id);
		
	}

	@Override
	public void delete(Agent agent) {
		
		agentRepository.delete(agent);

	}

	@Override
	public void deleteAll(Iterable<Agent> agents) {

		agentRepository.deleteAll(agents);
		
	}

	@Override
	public void deleteAll() {

		agentRepository.deleteAll();
		
	}

	@Override
	public Optional<Agent> findByUserId(Long user_id) {
	
		return agentRepository.findByUserId(user_id);
	}

}