package com.lydia.uniguideapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lydia.uniguideapi.domains.Career;
import com.lydia.uniguideapi.repositories.CareerRepository;


@Service
public class CareerServiceImpl implements CareerService {

	@Autowired
	CareerRepository careerRepository;	
	
	@Override
	public Career save(Career career) {
		
		return careerRepository.save(career);
		
	}


	@Override
	public Iterable<Career> saveAll(Iterable<Career> career) {
		
		return careerRepository.saveAll(career);
		
	}

	@Override
	public Optional<Career> findById(String id) {

		return careerRepository.findById(id);
	}

	@Override
	public boolean existsById(String id) {
		
		return careerRepository.existsById(id);
		
	}

	@Override
	public Iterable<Career> findAll() {
		
		return careerRepository.findAll();
		
	}

	@Override
	public Iterable<Career> findAllById(Iterable<String> ids) {

		return careerRepository.findAllById(ids);
		
	}

	@Override
	public long count() {
		
		return careerRepository.count();
		
	}

	@Override
	public void deleteById(String id) {
		
		careerRepository.deleteById(id);
		
	}

	@Override
	public void delete(Career career) {
		
		careerRepository.delete(career);

	}

	@Override
	public void deleteAll(Iterable<Career> career) {

		careerRepository.deleteAll(career);
		
	}

	@Override
	public void deleteAll() {

		careerRepository.deleteAll();
		
	}
}
