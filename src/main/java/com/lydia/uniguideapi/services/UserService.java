package com.lydia.uniguideapi.services;

import java.util.Optional;

import com.lydia.uniguideapi.domains.User;

public interface UserService{

	Optional<User> findById(Long id);
	Optional<User> findUserByUsername(String username);
	User saveUser(User user, String role);
}
