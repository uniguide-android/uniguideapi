package com.lydia.uniguideapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lydia.uniguideapi.domains.University;
import com.lydia.uniguideapi.repositories.UniversityRepository;


@Service
public class UniversityServiceImpl implements UniversityService {
	@Autowired
	UniversityRepository universityRepository;
	
	
	
	@Override
	public University save(University university) {
		
		return universityRepository.save(university);
		
	}

	@Override
	public Iterable<University> saveAll(Iterable<University> universities) {
		
		return universityRepository.saveAll(universities);
		
	}

	@Override
	public Optional<University> findById(Long id) {

		return universityRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		
		return universityRepository.existsById(id);
		
	}

	@Override
	public Iterable<University> findAll() {
		
		return universityRepository.findAll();
		
	}

	@Override
	public Iterable<University> findAllById(Iterable<Long> ids) {

		return universityRepository.findAllById(ids);
		
	}

	@Override
	public long count() {
		
		return universityRepository.count();
		
	}

	@Override
	public void deleteById(Long id) {
		
		universityRepository.deleteById(id);
		
	}

	@Override
	public void delete(University university) {
		
		universityRepository.delete(university);

	}

	@Override
	public void deleteAll(Iterable<University> universities) {

		universityRepository.deleteAll(universities);
		
	}

	@Override
	public void deleteAll() {

		universityRepository.deleteAll();
		
	}

	@Override
	public Optional<University> findByUserId(Long user_id) {
		
		return universityRepository.findByUserId(user_id);
	}

}