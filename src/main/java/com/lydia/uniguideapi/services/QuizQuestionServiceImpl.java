package com.lydia.uniguideapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.lydia.uniguideapi.domains.QuizQuestion;
import com.lydia.uniguideapi.repositories.QuizQuestionRepository;

public class QuizQuestionServiceImpl implements QuizQuestionService {

	@Autowired
	QuizQuestionRepository quizQuestionRepository;	
	
	@Override
	public QuizQuestion save(QuizQuestion career) {
		
		return quizQuestionRepository.save(career);
		
	}


	@Override
	public Iterable<QuizQuestion> saveAll(Iterable<QuizQuestion> career) {
		
		return quizQuestionRepository.saveAll(career);
		
	}

	@Override
	public Optional<QuizQuestion> findById(String id) {

		return quizQuestionRepository.findById(id);
	}

	@Override
	public boolean existsById(String id) {
		
		return quizQuestionRepository.existsById(id);
		
	}

	@Override
	public Iterable<QuizQuestion> findAll() {
		
		return quizQuestionRepository.findAll();
		
	}

	@Override
	public Iterable<QuizQuestion> findAllById(Iterable<String> ids) {

		return quizQuestionRepository.findAllById(ids);
		
	}

	@Override
	public long count() {
		
		return quizQuestionRepository.count();
		
	}

	@Override
	public void deleteById(String id) {
		
		quizQuestionRepository.deleteById(id);
		
	}

	@Override
	public void delete(QuizQuestion career) {
		
		quizQuestionRepository.delete(career);

	}

	@Override
	public void deleteAll(Iterable<QuizQuestion> career) {

		quizQuestionRepository.deleteAll(career);
		
	}

	@Override
	public void deleteAll() {

		quizQuestionRepository.deleteAll();
		
	}
}
