package com.lydia.uniguideapi.services;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lydia.uniguideapi.domains.Post;
import com.lydia.uniguideapi.repositories.PostRepository;

@Service
public class PostServiceImpl implements PostService {
	@Autowired
	PostRepository postRepository;
	
	@Override
	public Post save(Post post) {
		
		return postRepository.save(post);
		
	}

	@Override
	public Iterable<Post> saveAll(Iterable<Post> posts) {
		
		return postRepository.saveAll(posts);
		
	}

	@Override
	public Optional<Post> findById(Long id) {

		return postRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		
		return postRepository.existsById(id);
		
	}
	
	@Override
	public Iterable<Post> findAll() {
		
		return postRepository.findAll();
		
	}

	@Override
	public Iterable<Post> findAllById(Iterable<Long> ids) {

		return postRepository.findAllById(ids);
		
	}
	
	@Override
	public Iterable<Post> findByType(String type){
		
		return postRepository.findByType(type);
		
	}

	@Override
	public long count() {
		
		return postRepository.count();
		
	}

	@Override
	public void deleteById(Long id) {
		
		postRepository.deleteById(id);
		
	}

	@Override
	public void delete(Post post) {
		
		postRepository.delete(post);

	}

	@Override
	public void deleteAll(Iterable<Post> posts) {

		postRepository.deleteAll(posts);
		
	}

	@Override
	public void deleteAll() {

		postRepository.deleteAll();
		
	}

}
