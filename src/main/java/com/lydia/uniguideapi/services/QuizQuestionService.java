package com.lydia.uniguideapi.services;

import java.util.Optional;

import com.lydia.uniguideapi.domains.Career;
import com.lydia.uniguideapi.domains.QuizQuestion;


public interface QuizQuestionService {
	public QuizQuestion save(QuizQuestion quizQuestion);

	public Iterable<QuizQuestion> saveAll(Iterable<QuizQuestion> quizQuestion);

	Optional<QuizQuestion> findById(String id);

	boolean existsById(String id);
	
	Iterable<QuizQuestion> findAll();

	Iterable<QuizQuestion> findAllById(Iterable<String> ids);

	long count();
	
	void deleteById(String id);
	
	void delete(QuizQuestion quizQuestion);
	
	void deleteAll(Iterable<QuizQuestion> quizQuestion);

	void deleteAll();
}
