package com.lydia.uniguideapi.services;

import java.util.Optional;

import com.lydia.uniguideapi.domains.Career;

public interface CareerService {

	public Career save(Career career);
	
	public Iterable<Career> saveAll(Iterable<Career> career);

	Optional<Career> findById(String id);

	boolean existsById(String id);
	
	Iterable<Career> findAll();

	Iterable<Career> findAllById(Iterable<String> ids);

	long count();
	
	void deleteById(String id);
	
	void delete(Career career);
	
	void deleteAll(Iterable<Career> career);

	void deleteAll();
}
