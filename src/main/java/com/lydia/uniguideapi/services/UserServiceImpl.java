package com.lydia.uniguideapi.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lydia.uniguideapi.domains.Role;
import com.lydia.uniguideapi.domains.User;
import com.lydia.uniguideapi.repositories.RoleRepository;
import com.lydia.uniguideapi.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	@Autowired
	private UserRepository userRepository;
	@Autowired
    private RoleRepository roleRepository;  
	
	public Optional<User> findById(Long id) {
    	return userRepository.findById(id);
    }

    public Optional<User> findUserByUsername(String username) {
    	return userRepository.findByUsername(username);
    }
    
    public User saveUser(User user, String role) {
        Role userRole;
        if(role == "STUDENT") {
        	userRole = roleRepository.findByRole("STUDENT");
        }else if(role == "AGENT") {
        	userRole = roleRepository.findByRole("AGENT");
        }else {
        	userRole = roleRepository.findByRole("UNIVERSITY");
        }      
        
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        return userRepository.save(user);
    }

}
