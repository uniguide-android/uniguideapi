package com.lydia.uniguideapi.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.lydia.uniguideapi.domains.Post;

public interface PostService {
	public Post save(Post post);
	
	public Iterable<Post> saveAll(Iterable<Post> posts);

	Optional<Post> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Post> findAll();

	Iterable<Post> findAllById(Iterable<Long> ids);
	
	Iterable<Post> findByType(String type);

	long count();
	
	void deleteById(Long id);
	
	void delete(Post post);
	
	void deleteAll(Iterable<Post> posts);

	void deleteAll();
}


